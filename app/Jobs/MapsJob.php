<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use App\Facades\Maps;
use App\Modules\Maps\Responses\Success;
use App\Models\Route;
use App\Modules\Maps\Responses\Error;


class MapsJob extends Job
{
    protected $request;
    protected $idRoute;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request, $idRoute)
    {
        $this->request = $request;
        $this->idRoute = $idRoute;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $result = Maps::call($this->request);
        } catch (\Exception $e) {
            $result = new Error($e->getMessage(), 500);
        }
        $this->updateResponse($result);
    }

    /**
     * updateResponse
     *
     * @param mixed $result
     * @return void
     */
    protected function updateResponse($resultMaps)
    {
        $route = Route::where('id', $this->idRoute)->first();
        $result = $resultMaps->getMessage();
        $success = false;
        if ($resultMaps instanceof Success) {
            $result = json_encode($resultMaps->getObjectResponse());
            $success = true;
        }
        $route->route = $result;
        $route->success = $success;
        $route->save();
    }
}
