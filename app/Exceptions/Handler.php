<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        //return parent::render($request, $e);
        switch (true) {
        case ($e instanceof MethodNotAllowedHttpException):
            $status = Response::HTTP_METHOD_NOT_ALLOWED;
            $message = 'Method Not Allowed';
            break;
        case ($e instanceof NotFoundHttpException):
            $status = Response::HTTP_NOT_FOUND;
            $message = 'Not Found';
            break;
        case ($e instanceof AuthorizationException):
            $status = Response::HTTP_FORBIDDEN;
            $message = 'Forbidden';
            break;
        case ($e instanceof MapsException):
            $status = Response::HTTP_FORBIDDEN;
            $message = $e->getMessage();
            break;
        default:
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = 'Internal Server Error';
            break;
        }
        return new JsonResponse([
            "status" => "failure",
            "error" => $message
        ], $status);
    }
}
