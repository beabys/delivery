<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Maps\Google\Maps;

class MapsServiceProvider extends ServiceProvider
{
    const BUILDER = 'maps';

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind(static::BUILDER, function () {
            return new Maps();
        });
    }
}
