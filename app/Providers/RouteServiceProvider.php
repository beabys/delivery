<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use mmghv\LumenRouteBinding\RouteBindingServiceProvider as BaseServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\Route as Model;

/**
 * Class RouteServiceProvider
 * @package App\Providers
 */
class RouteServiceProvider extends BaseServiceProvider
{
    const TOKEN = 'token';
    const ROUTE = 'route';

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        // The binder instance
        $binder = $this->binder;
        static::bindings($binder);
    }

    /**
     *  Bindings
     */
    protected static function bindings($binder)
    {
        $binder->bind(self::ROUTE, function ($value) {
            $model = Model::where('uuid', $value)->first();
            if (!$model) {
                throw new NotFoundHttpException;
            }
            return $model;
        });
    }
}
