<?php

namespace App\Modules\Maps\Google\Messages;

use Illuminate\Http\Request;
use GuzzleHttp\Client;


class MessageRequest
{

    public $request;
    public $endpoint;
    public $key;
    public $units;

    /**
     * __construct
     *
     * @param Request $request
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->setConfig();
    }

    /**
     * setConfig
     *
     * @return void
     */
    protected function setConfig()
    {
        $config = config('google-maps.api');
        $this->endpoint = $config['endpoint'];
        $this->key = $config['key'];
        $this->units = $config['units'];
    }

    /**
     * send
     *
     * @return void
     */
    public function send()
    {
        $client = new Client();
        $response = $client->request('GET', $this->endpoint . '?' . $this->buildRequest());
        return $response->getBody()->getContents();
    }

    /**
     * buildRequest
     *
     * @param Request $request
     * @return void
     */
    protected function buildRequest()
    {
        $arrayData = [];
        $request = $this->request;
        foreach ($request as $data) {
            $newData = is_array($data) ? implode(',', $data) : $data;
            array_push($arrayData, $newData);
        }
        $vars = [
            'origins' => implode('|', $arrayData),
            'destinations' => implode('|', $arrayData),
            'sensor' => 'false',
            'key' => $this->key
        ];
        return http_build_query($vars, null, '&');
    } 
}