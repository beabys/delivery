<?php

namespace App\Modules\Maps\Google;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;
use App\Exceptions\MapsException;
use Illuminate\Support\Facades\Log;
use App\Modules\Maps\Google\Messages\MessageRequest;
use App\Modules\Maps\Responses\Success;

class Maps
{

    /**
     * call
     *
     * @param Request $request
     * @return void
     */
    public function call(array $request)
    {
        $requestMessage = new MessageRequest($request);
        $result = $requestMessage->send();
        return $this->processResult($request, $result);
    }

    /**
     * processResult
     *
     * @param mixed $result
     * @return void
     */
    protected function processResult($requestData, $result)
    {
        $route = [];
        $result = json_decode($result, true);
        //return $result;
        $rows = $this->getRows($result);
        array_push($route, ['key' => 0, 'distance' => 0, 'duration' => 0]);
        $bestRoute = $this->getBestRoute($rows, 0, $route);
        return $this->getResponseData($requestData, $bestRoute);

    }

    /**
     * getRows
     *
     * @param array $data
     * @return void
     */
    protected function getRows(array $data)
    {   
        $rows = array_get($data, 'rows', false);
        $newRows = [];
        $statusRows = $rows && $this->isOkStatus($data) ? array_pluck($rows, 'elements') : false;
        if (!$statusRows) {
            throw new MapsException(array_get($data, 'status'));
        }
        foreach ($statusRows as $statusRowKey => $statusRowValue) {
            $statuses = array_pluck($statusRowValue, 'status');
            $filtered = array_where($statuses, function ($value, $key) {
                return $value != 'OK';
            });
            if ($filtered) {
                throw new MapsException(head($filtered));
            }
            //set new element of the array
            foreach($statusRowValue as $key => $value)
            {
                $newRows[$statusRowKey][$key] = $statusRows[$key][$statusRowKey];
            }
            //remove distnce to same point selected
            unset($newRows[$statusRowKey][$statusRowKey]);
        }
        return $newRows;
    }

    /**
     * isOkStatus
     *
     * @param array $data
     * @return void
     */
    protected function isOkStatus(array $data)
    {
        $status = array_get($data, 'status', false);
        return ($status && $status == 'OK');
    }


    /**
     * getBestRoute
     *
     * @param array $rows
     * @param mixed $initialKey
     * @param mixed array
     * @param mixed array
     * @return void
     */
    protected function getBestRoute(array $rows, $initialKey, array $route = [], array $selectedKeys = [])
    {
        array_push($selectedKeys, $initialKey);
        //remove procesed values
        foreach ($selectedKeys as $key)
        {
            unset($rows[$initialKey][$key]);
        }
        $next = array_sort($rows[$initialKey], function ($value) {
            return array_get($value, 'distance.value');
        });
        reset($next); // make sure array pointer is at first element
        $next = key($next);
        if (!empty($next)) {
            $distance = array_get($rows, implode('.', [$initialKey, $next, 'distance', 'value']));
            $duration = array_get($rows, implode('.', [$initialKey, $next, 'duration', 'value']));
            array_push($route, ['key' => $next, 'distance' => $distance, 'duration' => $duration]);
        }
        //remove element processed
        unset($rows[$initialKey]);
        if (empty($rows)) {
            return $route;
        }
        return $this->getBestRoute($rows, $next, $route, $selectedKeys);
    }

    /**
     * getResponseData
     *
     * @param mixed $requestData
     * @param mixed $bestRoute
     * @return void
     */
    protected function getResponseData($requestData, $bestRoute)
    {;
        $path = [];
        $total_distance = 0;
        $total_time = 0;
        foreach($bestRoute as $keyRoute => $route) {
            array_push($path, [implode(', ', $requestData[$bestRoute[$keyRoute]['key']])]);
            $total_distance += $bestRoute[$keyRoute]['distance'];
            $total_time += $bestRoute[$keyRoute]['duration'];
        }
        $responseData = [
            'path' => $path,
            'total_distance' => $total_distance,
            'total_time' => $total_time
        ];
        return (new Success())->setObjectResponse($responseData);
    }
    
}
