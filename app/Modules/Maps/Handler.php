<?php

namespace App\Modules\Maps;

use App\Facades\Maps;
use Illuminate\Http\Request;
use App\Models\Route;
use App\Modules\Maps\Responses\Error;
use App\Jobs\MapsJob;
use Illuminate\Support\Facades\Queue;
use App\Modules\Maps\Responses\Success;


class Handler
{
    /**
     * request
     *
     * @param Request $request
     * @return void
     */
    public function request(Request $request)
    {
        $route = (new Route)->createRoute();
        Queue::push(new MapsJob($request->json()->all(), $route->id));
        return (new Success())->setObjectResponse($route);
    }
}