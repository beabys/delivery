<?php

namespace App\Modules\Maps\Responses;

class AbstractResponse
{

    protected $message;
    protected $status;
    protected $objectResponse;

    /**
     * __construct
     *
     * @param mixed $message
     * @param mixed $status
     * @return void
     */
    public function __construct($message = '', $status = 202)
    {
        $this->setMessage($message);
        $this->setStatus($status);
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of objectResponse
     */ 
    public function getObjectResponse()
    {
        return $this->objectResponse;
    }

    /**
     * Set the value of objectResponse
     *
     * @return  self
     */ 
    public function setObjectResponse($objectResponse)
    {
        $this->objectResponse = $objectResponse;

        return $this;
    }
}