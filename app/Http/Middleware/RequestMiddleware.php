<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class RequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->method() == 'GET') {
            return $next($request);
        }
        if (in_array($request->method(), ['POST']) && $request->isJson()) {
            $data = $request->json()->all();
            $request->request->replace(is_array($data) ? $data : []);
            return $next($request);
        }
        return new JsonResponse([
            'status' => 'failure',
            'error' => 'Invalid Format Data'
        ], 422);
    }
}
