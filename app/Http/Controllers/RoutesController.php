<?php

namespace App\Http\Controllers;

use App\Models\Route;
use Illuminate\Http\Request;
use App\Facades\Maps;
use Illuminate\Http\JsonResponse;
use App\Modules\Maps\Handler;
use App\Modules\Maps\Responses\Success;

class RoutesController extends Controller
{
    
    /**
     * show
     *
     * @param Route $route
     * @return void
     */
    public function show(Route $route)
    {
        $data = false;
        switch(true){
            case ($route->success == false && empty($route->route)):
                $statusMessage = 'in progress';
                break;
            case ($route->success == false && !empty($route->route)):
                $statusMessage = 'failure';
                $data = ['error' => $route->route];
                break;
            default:
                $statusMessage = 'success';
                $data = json_decode($route->route, true);
                break;
        }
        $response = ['status' => $statusMessage];
        if ($data) {
            $response = array_merge(['status' => $statusMessage], $data);
        }
        return new JsonResponse($response, 200);
    }

    /**
     * store
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $response = (new Handler())->request($request);
        $statusResponse = !is_null($response->getStatus()) ? $response->getStatus() : 200;
        $responseData = $response->getObjectResponse();
        $tittleMessage = ($response instanceof Success) ? 'token' : 'error';
        return new JsonResponse([
            'status' => 'success',
            $tittleMessage => $responseData->uuid,
        ], $statusResponse);
    }
}
