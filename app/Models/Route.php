<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class Route extends Model
{
    /**
     * @var array
     */
    protected $table = 'routes';

    /**
     * @var array
     */
    protected $casts = [
        'success' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * createRoute
     *
     * @return void
     */
    public function createRoute()
    {
        $uuid = Uuid::generate(3, Carbon::now()->timestamp, Uuid::NS_DNS)->string;
        $this->uuid = $uuid;
        $this->success = 0;
        $this->save();
        return $this;
    }
}
