<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Maps extends Facade
{
    /**
     * getFacadeAccessor
     */
    protected static function getFacadeAccessor()
    {
        return 'maps';
    }
}
