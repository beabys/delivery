Delivery
=============

## Prerequisites
* [Composer](https://getcomposer.org/)
* [Redis](https://redis.io/)
* [MySQL](https://www.mysql.com/) or [MariaDB](https://mariadb.org/)
* [Docker](https://www.docker.com/) (optional)


# Installing
### Set configurations

copy the .env.example to .env and fill the variables required for configuration

    cp .env.example .env

### Configuration Variables

#### Redis

    REDIS_HOST=
    REDIS_PASSWORD=
    REDIS_PORT=
    
| Parameter | Default Values |
| --- | --- |
| REDIS_HOST | 127.0.0.1 |
| REDIS_PASSWORD | null |
| REDIS_PORT | 6379 |


#### Google Maps Api

    GOOGLE_MAPS_API_ENDPOINT
    GOOGLE_MAPS_API_KEY
    GOOGLE_MAPS_API_DISTANCE_UNITS

| Parameter | Description |
| --- | --- |
| GOOGLE_MAPS_API_ENDPOINT | (default) endpoint of google Maps API |
| GOOGLE_MAPS_API_KEY | Api Key of google Maps API |
| GOOGLE_MAPS_API_DISTANCE_UNITS | imperial or metric |


### Docker Container configurations

copy the docker-compose.yaml.example to docker-compose.yaml and fill the variables required for configuration

    cp docker-compose.yaml.example docker-compose.yaml


Start the containers 

    docker-compose up -d

### Install Dependencies

    composer install
    
Composer can be run inside the container

    docker exec -it php_delivery composer install

### Generate Application Key
    
    php artisan key:generate
    
Inside the container

    docker exec -it php_delivery composer install

### Run Migrations
    
    php artisan migrate
    
Inside the container

    docker exec -it php_delivery php artisan migrate

### Runing Queues
If you want to star async service change the configuration in .env file as follow:
change from:

    QUEUE_DRIVER=sync
    
to: 

    QUEUE_DRIVER=redis
    
arter that execute the follow command to proccess the jobs in the Queue

    docker exec -it php_delivery php artisan queue:work
    
## Author

Created by Alfonso Rodriguez ([@beabys](http://twitter.com/beabys)).
