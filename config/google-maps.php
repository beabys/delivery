<?php

return [
    'api' => [
        'endpoint' => env('GOOGLE_MAPS_API_ENDPOINT', 'https://maps.googleapis.com/maps/api/distancematrix/json'),
        'key' => env('GOOGLE_MAPS_API_KEY', 6379),
        'units' => env('GOOGLE_MAPS_API_DISTANCE_UNITS', 'metric'),
    ],
];
